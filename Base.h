/* 
 * File:   Base.h
 * Author: vujevic
 *
 * Created on April 25, 2014, 11:41 PM
 */

#include<string>
#include<boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>
#include "SeqIndex.h"
#include "FindIndex.h"
#include "DatabaseElement.h"
#include "Seg.h"

using namespace std;
using namespace boost;
#ifndef BASE_H
#define	BASE_H

class Base {
public:

 
    void setBase(string pathToDatabase,string pathToIndexes,string pathToNumberOfCounts,Type type);
    string getPathToIndexes(void);

    void crawl(void);

    void readIndexes(void);

    void readNumberOfCounts(void);

    void checkIfIndexesExists(void);

    void checkIfNumberOfCountsExists(void);

    FindIndex searchTool;

    /**Mapa u koju se pohranjuju zapisi iz baze podataka pohranjenih pod ključem koji je redni broj zapisa u bazi.
     */
    unordered_map<long, DatabaseElement> baseInMemory;

	/**
     * Map where we store indexes for every sequence.
     */
    unordered_map<long, SeqIndex> indexes;
    unordered_map<string, unordered_set<long> >* getPentapeptidSequenceMap();

    unordered_map<long, SeqIndex>* getIndexes();

    void addLengthOfSequence(int n);

    void crawls();
    long long getDatabaseSize();
private:

    const char * pathToDatabase;

    const char * pathToIndexes;

    //path to file where we store frequency for same pentapeptide.
    const char * pathToNumberOfCounts;


    bool indexesExists;

    bool numberOfCountsExists;


    /**
     * Map where we store ID of sequences that is marked with pentapeptide which is key in map.
     */
    unordered_map<string, unordered_set<long> > pentapeptidSequenceMap;


    

    long long databaseSize;

    void writeIndexes();

    void writePentapeptidesFrequency();

    void readDatabase();

    void readInMemory();
    void readMaskedAndStoreIndexes();
    void makeIndexes();
};

#endif	/* BASE_H */
