#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Tachyon configuration
CND_PLATFORM_Tachyon=GNU-Linux-x86
CND_ARTIFACT_DIR_Tachyon=bin
CND_ARTIFACT_NAME_Tachyon=tachyon
CND_ARTIFACT_PATH_Tachyon=bin/tachyon
CND_PACKAGE_DIR_Tachyon=dist/Tachyon/GNU-Linux-x86/package
CND_PACKAGE_NAME_Tachyon=tachyon.tar
CND_PACKAGE_PATH_Tachyon=dist/Tachyon/GNU-Linux-x86/package/tachyon.tar
# Query configuration
CND_PLATFORM_Query=GNU-Linux-x86
CND_ARTIFACT_DIR_Query=bin
CND_ARTIFACT_NAME_Query=runquery
CND_ARTIFACT_PATH_Query=bin/runquery
CND_PACKAGE_DIR_Query=dist/Query/GNU-Linux-x86/package
CND_PACKAGE_NAME_Query=tachyon.tar
CND_PACKAGE_PATH_Query=dist/Query/GNU-Linux-x86/package/tachyon.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
