#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Tachyon
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/Base.o \
	${OBJECTDIR}/Seg.o\
	${OBJECTDIR}/DatabaseElement.o \
	${OBJECTDIR}/FindIndex.o \
	${OBJECTDIR}/SeqIndex.o \
	${OBJECTDIR}/Tachyon.o \
	${OBJECTDIR}/ScoreMatrix.o \
	${OBJECTDIR}/Swimd.o \
	${OBJECTDIR}/Evalue.o \
	${OBJECTDIR}/Scorer.o \
	${OBJECTDIR}/Constants.o

# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lpthread

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk bin/tachyon

bin/tachyon: ${OBJECTFILES}
	${MKDIR} -p bin
	${LINK.cc} -o bin/tachyon -I lib ${OBJECTFILES} ${LDLIBSOPTIONS}  -L lib/so -lboost_system -lboost_thread -Wl,-rpath=$(shell pwd)/lib/so

${OBJECTDIR}/Base.o: Base.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -O2 -std=c++11 -I lib -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Base.o Base.cpp

${OBJECTDIR}/DatabaseElement.o: DatabaseElement.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -O2 -std=c++11 -I lib -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DatabaseElement.o DatabaseElement.cpp

${OBJECTDIR}/FindIndex.o: FindIndex.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -O2 -std=c++11 -I lib -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/FindIndex.o FindIndex.cpp

${OBJECTDIR}/SeqIndex.o: SeqIndex.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -O2 -std=c++11 -I lib -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SeqIndex.o SeqIndex.cpp

${OBJECTDIR}/Tachyon.o: Tachyon.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -O2 -std=c++11 -I lib -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Tachyon.o Tachyon.cpp

${OBJECTDIR}/Seg.o: Seg.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -O2 -std=c++11 -I lib -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Seg.o Seg.cpp

${OBJECTDIR}/ScoreMatrix.o: ScoreMatrix.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -O2 -march=native -std=c++11 -I lib -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ScoreMatrix.o ScoreMatrix.cpp

${OBJECTDIR}/Swimd.o: Swimd.cpp Swimd.h 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -O2 -march=native -std=c++11 -I lib -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Swimd.o Swimd.cpp

${OBJECTDIR}/Constants.o: Constants.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -O2 -std=c++11 -I lib -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Constants.o Constants.cpp

${OBJECTDIR}/Evalue.o: Evalue.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -O2 -std=c++11 -I lib -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Evalue.o Evalue.cpp

${OBJECTDIR}/Scorer.o: Scorer.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -O2 -std=c++11 -I lib -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Scorer.o Scorer.cpp



# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} bin/tachyon

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
