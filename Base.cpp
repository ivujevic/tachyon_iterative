/* 
 * File:   Base.cpp
 * Author: vujevic
 * 
 * Created on April 25, 2014, 11:41 PM
 */

#include "Base.h"
#include<iostream>
#include<fstream>

#include<string.h>
#include "FindIndex.h"
#include<thread>
#include <boost/thread/thread.hpp>
#include "Constants.h"
#include<boost/asio.hpp>

using namespace std;

int numberOfRead = 0;

pthread_mutex_t mutexP;
pthread_mutex_t mutexCounter;
pthread_mutex_t makeI;

int completedSequence = 0;
Seg seg;
unordered_map<string, long> tempMapForCounts;
bool IndexesCompleted = false;
bool numCompleted = false;
int wordLength;
int listSize;
Type  t;


// the actual thread poolt
struct ThreadPool {
   ThreadPool(std::size_t);
   template<class F>
   void enqueue(F f);
   ~ThreadPool();
    void joinAll();
private:
   // the io_service we are wrapping
   boost::asio::io_service service;
   using asio_worker = std::unique_ptr<boost::asio::io_service::work>;
   asio_worker working;
   // need to keep track of threads so we can join them
   std::vector<std::unique_ptr<boost::thread>> workers;
};

// the constructor just launches some amount of workers
ThreadPool::ThreadPool(size_t threads)
   :service()
   ,working(new asio_worker::element_type(service))
{
   for ( std::size_t i = 0; i < threads; ++i ) {
      workers.push_back(
         std::unique_ptr<boost::thread>(
            new boost::thread([this]
            {
               service.run();
            })
         )
      );
   }
}

// add new work item to the pool
template<class F>
void ThreadPool::enqueue(F f) {
   service.post(f);
}
void ThreadPool::joinAll(){
	working.reset();
	service.run();
}
// the destructor joins all threads
ThreadPool::~ThreadPool() {
   working.reset();
   service.run();
}

void Base::setBase(string pDatabase, string pIndexes,string pNumber,Type type)
{
  pathToDatabase = pDatabase.c_str();
  pathToIndexes = pIndexes.c_str();
  pathToNumberOfCounts = pNumber.c_str();
  seg = Seg(type);
  if(type == Type::PROTEINS)
  {
    wordLength = proteinsWordLength;
    listSize = proteinsListSize;
  }else{
    wordLength = nucleotidesWordLength;
    listSize =   nucleotidesListSize;
  } 
  t = type;
  searchTool.setType(type);
}


string Base::getPathToIndexes() {
    return pathToIndexes;
}

void Base::addLengthOfSequence(int n) {
    databaseSize += n;
}

unordered_map<long, SeqIndex>* Base::getIndexes() {
    return &indexes;
}

unordered_map<string, unordered_set<long> >* Base::getPentapeptidSequenceMap() {
    return &pentapeptidSequenceMap;
}

void Base::checkIfIndexesExists() {
    ifstream stream(pathToIndexes);
    indexesExists = stream.good();
    stream.close();
}

void Base::checkIfNumberOfCountsExists() {
    ifstream stream(pathToNumberOfCounts);
    numberOfCountsExists = stream.good();
    stream.close();
}
long long Base::getDatabaseSize(){
  return databaseSize;
}
void Base::readIndexes() {
  if (indexesExists) {
	cout<<"postoje"<<endl;
    ifstream stream(pathToIndexes);
    string index;
    string temp = "";
    int ID = 0;
    vector<string> pentapeptides;

    while (getline(stream, index)) {
      SeqIndex si;

      for (int i = 0; i < index.length(); i++) {
        if (index[i] == ',') {
          pentapeptides.push_back(temp);
          temp.clear();
          continue;
        } else {
          temp += index[i];

        };

      }
      si.setPentapeptides(pentapeptides,listSize);
      indexes[ID] = si;
      ID++;
      pentapeptides.clear();
    }
    IndexesCompleted = true;
cout<<"Ucitavam parov"<<endl;
    for (unordered_map<long, SeqIndex>::iterator it = indexes.begin(); it != indexes.end(); it++) {
      vector<string> pentapeptides = (*it).second.getPentapeptides();
      if(pentapeptides.at(0) == "Q") continue;
      for (vector<string>::iterator it1 = pentapeptides.begin(); it1 != pentapeptides.end(); it1++) {
        pentapeptidSequenceMap[*it1].insert((*it).first);
      }
    }	
    stream.close();
    return;
  }
}

void Base::writeIndexes() {
    ofstream stream(pathToIndexes);
    string temp;

    long n = indexes.size();

    for (pair<long, SeqIndex> p : indexes) {
        temp.clear();
        vector<string> v = p.second.getPentapeptides();
        for (string s : v) {
            temp = s + ',';
        }
        stream << temp << endl;
    }

}

/**
 * Check if string contains lower case.
 */
bool containsLowercase(string niz) {
    for (int i = 0; i < niz.length(); i++) {
        char a = niz.at(i);
        if (a >= 'a' && a <= 'z') {
            return true;
        }
    }
    return false;
}

void countsPentapeptide(string sequence) {

  long sequenceLength = sequence.size();
  string previous = "";

  int startCodon = 0;
  int stopCodon = sequenceLength;
  string maskChar = "X";
  if(t == Type::NUCLEOTIDES) {
    maskChar = "N";
    for(int i = 0; i<sequenceLength - 2;i++){
      string codon = sequence.substr(i,3);
      if(codon == "AUG" || codon =="ATG"){
        startCodon = i;
        break;
      }
    }
    for(int i = startCodon;i<sequenceLength -2;i+=3) {
      string codon = sequence.substr(i,3);
      if(codon =="UAG" || codon =="TAG" || codon == "UAA" || codon =="TAA" || codon == "UGA" || codon == "TGA"){
        stopCodon = i;
      }
    }
  }
  for (int i = startCodon; i < stopCodon - (wordLength -1); i++) {

    string pentapeptide = sequence.substr(i, wordLength);

    //overlap words
    if (pentapeptide == previous) {
      continue;
    }
    previous = pentapeptide;
    
    if (pentapeptide.find("B") != string::npos) continue;
    if (pentapeptide.find("J") != string::npos) continue;
    if (pentapeptide.find("O") != string::npos) continue;
    if (pentapeptide.find("U") != string::npos) continue;
    if (pentapeptide.find("Z") != string::npos) continue;
    if (pentapeptide.find("X") != string::npos || (maskChar == "N") ? (pentapeptide.find(maskChar) != string::npos) : false) continue;
    
    pthread_mutex_lock(&mutexP);


    if (tempMapForCounts.find(pentapeptide) != tempMapForCounts.end()) {
      long value = tempMapForCounts[pentapeptide];
      value++;
      tempMapForCounts[pentapeptide] = value;
    } else {
      tempMapForCounts[pentapeptide] = 1;
    }

    pthread_mutex_unlock(&mutexP);
  }

}

void Base::readDatabase() {

  ifstream base(pathToDatabase);
  ofstream b("izlaz.fa");    
  string line;
  string sequence="";
  string description="";

  DatabaseElement element;
  int ID = 0;

  boost::thread_group workers;
  ThreadPool pool(50);
	cout<<"start read"<<endl;
  while( getline(base,line) ) 
  {
    if( line=="" ) continue;

    if( line.at(0) == '>' ) 
    {
      if( sequence != "")
      {
        int len = sequence.size();
        for(int i = 0; i < len; i++)
        {
          sequence.at(i) = toupper(sequence.at(i));
        }

        if( !numberOfCountsExists ) 
        {
          Seg seg = Seg(t);
	  string tempSeq = seg.getMasked(sequence);
          b<<">"+description +"\n"+tempSeq+"\n";
          pool.enqueue([tempSeq]
              {
              countsPentapeptide(tempSeq);
              });
        }
        addLengthOfSequence(len);
        element.setFirstLine(description);
        element.setSequence(sequence);
        element.setID(ID);
        baseInMemory[ID] = element;

        sequence = "";
        ID++;
      }
      description = line.substr(1);
    }else sequence +=line;
  }

  //Last element

  int len = sequence.size();
  for( int i = 0; i < len; i++) 
  {
    sequence.at(i) = toupper(sequence.at(i));
  }

  if( !numberOfCountsExists ) 
  {
    Seg seg = Seg(t);
    string tempSeq = seg.getMasked(sequence);
    b<<">"+description +"\n"+tempSeq+"\n";
    pool.enqueue([tempSeq]
        {
        countsPentapeptide(tempSeq);
        });
  }

  element.setFirstLine(description);
  element.setSequence(sequence);
  element.setID(ID);
  baseInMemory[ID] = element;

  if( !numberOfCountsExists ) 
  {
    pool.joinAll();
    b.close();
    searchTool.frequencyOfPentapeptide = tempMapForCounts;
  }
}

void Base::readNumberOfCounts() {
  ifstream file(pathToNumberOfCounts);

  string line;
  while (getline(file, line)) {

    size_t delimiter = line.find_first_of(',');
    string pentapeptid = line.substr(0, delimiter);

    int n = atoi(line.substr(delimiter + 1).c_str());

    searchTool.frequencyOfPentapeptide[pentapeptid] = n;
  }
  numCompleted = true;
}

void Base::writePentapeptidesFrequency() {
    ofstream file(pathToNumberOfCounts);
    for (pair <string, long> p : tempMapForCounts) {
        file << p.first << "," << p.second << endl;
        searchTool.frequencyOfPentapeptide[p.first] = p.second;
    }

    file.close();
    tempMapForCounts.clear();
}

void * write(void * arg) {
    Base *b = static_cast<Base *> (arg);
    ofstream stream((*b).getPathToIndexes().c_str());

    string temp;

    long size = (*(*b).getIndexes()).size();

    for (long i = 0; i < size; i++) {
        vector<string> vector = (*(*b).getIndexes()).at(i).getPentapeptides();
        temp.clear();
        for (string s : vector) {
            temp += s + ',';
        }
        stream << temp << endl;
    }
}
void f(Base* base,string sequence,long id,int count){
	SeqIndex i = (*base).searchTool.find(sequence);
	pthread_mutex_lock(&makeI);
	//cout<<"Stavljam"<<endl;
	(*base).indexes[id] = i;
	//cout<<"Gotov"<<endl;
	//if(count % 100000 == 0 || count > 26685800) 
	pthread_mutex_unlock(&makeI);
}
void Base::makeIndexes(void){
  ThreadPool pool(50); 
  int count = 0;
  ifstream base("izlaz.fa");
  string line; 
  string sequence;
  int id = 0;
  string description; 
  while( getline(base,line) ) 
  {
    if( line=="" ) continue;

    if( line.at(0) == '>' ) 
    {
      if( sequence != "")
      {

        pool.enqueue([this,sequence,id,count]
            {
            f(this,sequence,id,count);
            });
        sequence = "";
        id++;
      }
      description = line.substr(1);
    }else sequence +=line;
  }
  pool.enqueue([this,sequence,id,count]
      {
      f(this,sequence,id,count);
      });

  pool.joinAll();
}


void Base::crawls(void) {
  
  boost::thread_group group;
  group.add_thread( new boost::thread(boost::bind(&Base::readIndexes,this)) ); // thread for read indexes
  group.add_thread( new boost::thread(boost::bind(&Base::readNumberOfCounts,this)) ); // thread for read number of counts
  
  readDatabase(); //read database in memory, if number of counts doesn't exist, create
  cout<<"gotov s citanjem"<<endl;
  group.join_all();
  pthread_t writeIndexesThread;
  int f = 0; //writeIndexesThread start
  if (!numberOfCountsExists) {

    writePentapeptidesFrequency();
  }
  if (!indexesExists) {
    makeIndexes();

    f = 1;

    pthread_create(&writeIndexesThread, NULL, write, (void *) this);

    for (unordered_map<long, SeqIndex>::iterator it = indexes.begin(); it != indexes.end(); it++) {
      vector<string> pentapeptides = (*it).second.getPentapeptides();
      if(pentapeptides.at(0) == "Q") continue;
      for (vector<string>::iterator it1 = pentapeptides.begin(); it1 != pentapeptides.end(); it1++) {
        pentapeptidSequenceMap[*it1].insert((*it).first);
      }
    }
  }

  if (f == 1) pthread_join(writeIndexesThread, NULL);
}
