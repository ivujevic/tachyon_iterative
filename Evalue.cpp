#include <math.h>
#include<iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "Constants.h"
#include "Scorer.h"
#include "Evalue.h"

#define MAX(a, b) ((a) > (b) ? (a) : (b))

#define SCORER_CONSTANTS_LEN (sizeof(scorerConstants) / sizeof(ScorerConstants))

using namespace std;
struct EValueParams {
    double lambda;
    double K;
    double H;
    double a;
    double C;
    double alpha;
    double sigma;
    double b;
    double beta;
    double tau;
    double G;
    double aUn;
    double alphaUn;
    long long length;
};

typedef struct ScorerConstants {
    const char* matrix;
    int gapOpen;
    int gapExtend;
    double lambda;
    double K;
    double H;
    double a;
    double C;
    double alpha;
    double sigma;
} ScorerConstants;


// lambda, k, H, a, C, Alpha, Sigma
static ScorerConstants scorerConstants[] = {
     {"BLOSUM_50",-1, -1, 0.2318, 0.112, 0.3362, 0.6895, 0.609639, 5.388310, 5.388310},
     {"BLOSUM_50",13, 3,0.212, 0.063, 0.19, 1.1, 0.639287, 18.113800, 18.202800},
     {"BLOSUM_50",12, 3,0.206, 0.055, 0.17, 1.2,0.644715, 22.654600, 22.777700},
     {"BLOSUM_50",11, 3,0.197, 0.042, 0.14, 1.4, 0.656327, 29.861100, 30.045700},
     {"BLOSUM_50",10, 3 ,0.186, 0.031, 0.11, 1.7, 0.671150, 42.393800, 42.674000},	
     {"BLOSUM_50",9, 3,0.172, 0.022, 0.082, 2.1, 0.694326, 66.069600, 66.516400},
     {"BLOSUM_50",16, 2,0.215, 0.066, 0.20, 1.05, 0.633899, 17.951800, 18.092100},
     {"BLOSUM_50",15, 2,0.210, 0.058, 0.17, 1.2,  0.641985, 21.940100, 22.141800},
     {"BLOSUM_50",14, 2,0.202, 0.045, 0.14, 1.4,  0.650682, 28.681200, 28.961900},
     {"BLOSUM_50",13, 2,0.193, 0.035, 0.12, 1.6,  0.660984, 42.059500, 42.471600},
     {"BLOSUM_50",12, 2,0.181, 0.025, 0.095, 1.9, 0.678090, 63.747600, 64.397300},
     {"BLOSUM_50",19, 1,0.212, 0.057, 0.18, 1.2,  0.635714, 26.311200, 26.923300},
     {"BLOSUM_50",18, 1,0.207, 0.050, 0.15, 1.4,  0.643523, 34.903700, 35.734800},
     {"BLOSUM_50",17, 1,0.198, 0.037, 0.12, 1.6,  0.654504, 48.895800, 50.148600},
     {"BLOSUM_50",16, 1,0.186, 0.025, 0.10, 1.9,  0.667750, 76.469100, 78.443000},
     {"BLOSUM_50",15, 1,0.171, 0.015, 0.063, 2.7, 0.694575, 140.053000, 144.160000},
 };



/*{
    { "BLOSUM_50", -1, -1, 0.2318, 0.112, 0.3362, 0.6895, 0.609639, 5.388310, 5.388310 },
    { "BLOSUM_62", 11, 2, 0.297, 0.082, 0.27, 1.1, 0.641766, 12.673800, 12.757600 },
    { "BLOSUM_50", 10, 3, 0.158, 0.019, 0.100, 1.7, 0.671150,42.393800 , 42.674000 },
    { "BLOSUM_62", 9, 2, 0.279, 0.058, 0.19, 1.5, 0.659245, 22.751900, 22.950000 },
    { "BLOSUM_62", 8, 2, 0.264, 0.045, 0.15, 1.8, 0.672692, 35.483800, 35.821300 },
    { "BLOSUM_62", 7, 2, 0.239, 0.027, 0.10, 2.5, 0.702056, 61.238300, 61.886000 },
    { "BLOSUM_62", 6, 2, 0.201, 0.012, 0.061, 3.3, 0.740802, 140.417000, 141.882000 },
    { "BLOSUM_62", 13, 1, 0.292, 0.071, 0.23, 1.2, 0.647715, 19.506300, 19.893100 },
    { "BLOSUM_62", 12, 1, 0.283, 0.059, 0.19, 1.5, 0.656391, 27.856200, 28.469900 },
    { "BLOSUM_62", 11, 1, 0.267, 0.041, 0.14, 1.9, 0.669720, 42.602800, 43.636200 },
    { "BLOSUM_62", 10, 1, 0.243, 0.024, 0.10, 2.5, 0.693267, 83.178700, 85.065600 },
    { "BLOSUM_62", 9, 1, 0.206, 0.010, 0.052, 4.0, 0.731887, 210.333000, 214.842000 },
};*/


EValueParams* createEValueParams(long long databaseLen, Scorer* scorer) {
    
    const char* matrix = scorerGetName(scorer);
    int gapOpen = scorerGetGapOpen(scorer);
    int gapExtend = scorerGetGapExtend(scorer);
    
    double alphaUn = scorerConstants[0].alpha;
    double aUn = scorerConstants[0].a;
    double G = gapOpen + gapExtend;
    
    int index = -1;
    
    for (int i = 0; i < SCORER_CONSTANTS_LEN; ++i) {

        ScorerConstants entry = scorerConstants[i];
        
        if (entry.gapOpen == gapOpen && entry.gapExtend == gapExtend &&
            strncmp(entry.matrix, matrix, strlen(entry.matrix)) == 0) {
            index = i;
            break;
        }
    }
    
    if (index == -1) {
        index = 0;
    }
    
    EValueParams* params = (EValueParams*) malloc(sizeof(struct EValueParams));
    
    params->G = G;
    params->aUn = aUn;
    params->alphaUn = alphaUn;
    params->lambda = scorerConstants[index].lambda;
    params->K = scorerConstants[index].K;
    params->H = scorerConstants[index].H;
    params->a = scorerConstants[index].a;
    params->C = scorerConstants[index].C;
    params->alpha = scorerConstants[index].alpha;
    params->sigma = scorerConstants[index].sigma;
    params->b = 2.0 * G * (params->aUn - params->a);
    params->beta = 2.0 * G * (params->alphaUn - params->alpha);
    params->tau = 2.0 * G * (params->alphaUn - params->sigma);
    params->length = databaseLen;
    
    return params;
}

long double calculateEValue(int score, int queryLen, int targetLen, EValueParams* params) {
    
    // code taken from blast
    // pile of statistical crap
    int y_ = score;
    int m_ = queryLen;
    int n_ = targetLen;
    
    // the pair-wise e-value must be scaled back to db-wise e-value
    long double db_scale_factor = (long  double) params->length / (double) n_;

   long  double lambda_    = params->lambda;
    long  double k_         = params->K;
   long   double ai_hat_    = params->a;
    long  double bi_hat_    = params->b;
   long   double alphai_hat_= params->alpha;
    long  double betai_hat_ = params->beta;
    long  double sigma_hat_ = params->sigma;
    long  double tau_hat_   = params->tau;

    // here we consider symmetric matrix only
    long  double aj_hat_    = ai_hat_;
    long  double bj_hat_    = bi_hat_;
    long  double alphaj_hat_= alphai_hat_;
    long  double betaj_hat_ = betai_hat_;

    // this is 1/sqrt(2.0*PI)
    static double const_val = 0.39894228040143267793994605993438;
    long  double m_li_y, vi_y, sqrt_vi_y, m_F, P_m_F;
    long  double n_lj_y, vj_y, sqrt_vj_y, n_F, P_n_F;
    long  double c_y, p1, p2;
    long  double area;

    m_li_y = m_ - (ai_hat_*y_ + bi_hat_);
    vi_y = MAX(2.0*alphai_hat_/lambda_, alphai_hat_*y_+betai_hat_);
    sqrt_vi_y = sqrt(vi_y);
    m_F = m_li_y/sqrt_vi_y;
    P_m_F = 0.5 + 0.5 * erf(m_F);
    p1 = m_li_y * P_m_F + sqrt_vi_y * const_val * exp(-0.5*m_F*m_F);

    n_lj_y = n_ - (aj_hat_*y_ + bj_hat_);
    vj_y = MAX(2.0*alphaj_hat_/lambda_, alphaj_hat_*y_+betaj_hat_);
    sqrt_vj_y = sqrt(vj_y);
    n_F = n_lj_y/sqrt_vj_y;
    P_n_F = 0.5 + 0.5 * erf(n_F);
    p2 = n_lj_y * P_n_F + sqrt_vj_y * const_val * exp(-0.5*n_F*n_F);

    c_y = MAX(2.0*sigma_hat_/lambda_, sigma_hat_*y_+tau_hat_);
    area = p1 * p2 + c_y * P_m_F * P_n_F;
    
    return area*k_ * exp(-lambda_ * y_) * db_scale_factor;
}
