
/* 
 * File:   FindIndex.cpp
 * Author: vujevic
 * 
 * Created on April 25, 2014, 11:42 PM
 */

#include "FindIndex.h"
#include "Constants.h"
#include <iostream>


void FindIndex::setType(Type type){
  this->type = type;
  if(type == Type::PROTEINS) 
  {
    wordLength = proteinsWordLength;
    listSize = proteinsListSize;
  }else {
    wordLength = nucleotidesWordLength;
    listSize = nucleotidesListSize;
  }
}

/**
 * Check if string contains lower case.
 */
bool containsLowercase1(string niz) {
    for (int i = 0; i < niz.length(); i++) {
        char a = niz.at(i);
        if (a >= 'a' && a <= 'z') {
            return true;
        }
    }
    return false;
}

bool cmp(const pair<string, long> & p1, const pair<string, long> &p2) {
    if (p1.second == p2.second) {
        return p1.second < p2.second;
    }
    return p1.second > p2.second;
}


SeqIndex FindIndex::find(string sequence) {

  if(frequencyOfPentapeptide.size() == 0 ) std::cout<<"Error!!!!!"<<endl;
  //use for keeping the order of the input pentapeptide
  vector<string> inputPentapeptides;


  unordered_map <string, long> pentapeptides;



  long seqLength = sequence.size();


	int startCodon = 0;
	int stopCodon = seqLength;

	if(type != Type::PROTEINS) {
                for(int i = 0; i<seqLength - 2;i++){
                   string codon = sequence.substr(i,3);
                   if(codon == "AUG" || codon =="ATG"){
                        startCodon = i;
                        break;
                  }
                }
                for(int i = startCodon;i<seqLength -2;i+=3) {
                        string codon = sequence.substr(i,3);
                        if(codon =="UAG" || codon =="TAG" || codon == "UAA" || codon =="TAA" || codon == "UGA" || codon == "TGA"){
                                stopCodon = i;
                        }
                }
        }
	
  //odredi koliko se puta svaki pentapeptid pojavljuje

  for (int i = startCodon; i < stopCodon - (wordLength -1); i += wordLength) {

    string pentapeptide = sequence.substr(i, wordLength);

    //nije normalan biološki slijed tada ne postoji x u nizu
    if(pentapeptide.find("B") != string::npos) continue;
    if(pentapeptide.find("J") != string::npos) continue;
    if(pentapeptide.find("O") != string::npos) continue;
    if(pentapeptide.find("U") != string::npos) continue;
    if(pentapeptide.find("Z") != string::npos) continue;
    if (pentapeptide.find("N") != string::npos || containsLowercase1(pentapeptide) || pentapeptide.find("X") != string::npos) {
      continue;
    }

    //U slučaju da se pentapeptid do sada nije pojavljivao dodaj u vektor
    if (pentapeptides.find(pentapeptide) == pentapeptides.end()) {
      inputPentapeptides.push_back(pentapeptide);
      pentapeptides[pentapeptide] = frequencyOfPentapeptide[pentapeptide];
    }

  }

  //vekor u kojem ćemo dobiti sortirane pentapeptide
  vector< pair<string, long> > sortedPentapeptides;


  //make new vector for sort with keeping input order.

  for (vector<string>::iterator it = inputPentapeptides.begin(); it != inputPentapeptides.end(); it++) {
    sortedPentapeptides.push_back(pair<string, long> (*it, pentapeptides[*it]));
  }
  //dodadj u novi vektor pentapeptide i njihovo pojavljivanje te ga sortiraj

  stable_sort(sortedPentapeptides.begin(), sortedPentapeptides.end(), cmp);

  pentapeptides.clear();
  inputPentapeptides.clear();

  //vektor koji predstavlja 5 pentapeptida koji određuju sekvencu
  vector <string> findedPentapeptides;

  int ucitano = 0;
  for (vector< pair<string, long> >::iterator it = sortedPentapeptides.begin(); it != sortedPentapeptides.end() && ucitano < listSize; it++) {
    findedPentapeptides.push_back((*it).first);
    ucitano++;
  }


  SeqIndex a;
  a.setPentapeptides(findedPentapeptides,listSize);
  return a;
}
