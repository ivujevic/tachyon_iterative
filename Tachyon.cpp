#include <iostream>
#include <stdio.h>
#include<stdlib.h>
#include<set>
#include <sys/types.h>
#include <sys/stat.h>
#include<ctime>
#include<algorithm>
#include<vector>
#include<pthread.h>
#include<fstream>
#include<time.h>
#include<boost/unordered_map.hpp>
#include<boost/unordered_set.hpp>
#include <boost/asio/io_service.hpp>
#include "Base.h"
#include "Constants.h"
#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/thread/thread.hpp>
#include <boost/lockfree/queue.hpp>
#include <queue>

#include "Swimd.h"
#include "ScoreMatrix.hpp"
#include "Evalue.h"
#include "Scorer.h"
int NUM_OF_CORES = sysconf(_SC_NPROCESSORS_ONLN) - 1;

double Score_Cutoff = 0.015;

int numberOfSequences = 0;
int maxDepth = 10;
double tl = 0.1;
double ts = 0.4;

double MAX_EVALUE  = 0.015;
EValueParams* eValueParams;

string outDir;

using namespace std;
using boost::unordered_map;
using boost::unordered_set;
using boost::asio::ip::tcp;

boost::lockfree::queue<int> queuePosition(500);
vector< pair<string,string> > queueQuery;
int minHits;
int word_length;
string maskChar = "X"; 
Base base;

//Map where we store ID of sequences that is marked with pentapeptide which is key in map.
unordered_map<string, unordered_set<long> >* pentapeptideSequenceMap = base.getPentapeptidSequenceMap();

bool usporedi(const pair<long, long double> & p1, const pair<long, long double> &p2) {
    if (p1.second == p2.second) {
        return p1.second > p2.second;
    }
    return p1.second < p2.second;
}

bool cmpa(const pair<long, float> &p1, const pair<long, float> &p2) {
    return p1.second > p2.second;
}

void readDatabase(void) {
    base.crawls();
}

void printHelp() {
    fprintf(stderr, "\
       Usage: tachyon [-s settings] [-n -p]\n\
                 -s \n\
                       filename containing paths to settings.\n\
                                       First line path to original database\n\
                                       Second line path to file of indexes,\n\
                                       Third line path to file of number of pentapeptides\n\
                  [-n -p] -n for nucleotides database\n\
                          -p for proteins database\n\n");
}



float getScore(unordered_set<string>* peptides, unordered_set<string>* queryPeptides)
{
  int numberOfCommon = 0;
  int peptidesSize = 0;
  for(string w : (*peptides))
  {
    peptidesSize ++;
    if( (*queryPeptides).find(w) != (*queryPeptides).end())
    {
      numberOfCommon++;
    }
  }
  if(numberOfCommon < 3 ) return 0.0;

  int querySize = (*queryPeptides).size();
  int totalNumberOfPeptides = numberOfCommon + ( peptidesSize - numberOfCommon) + ( querySize - numberOfCommon);
  return ( (float) numberOfCommon / (float) totalNumberOfPeptides);
}

void getListOfPentapeptides(unordered_set<string>* pentapeptides, string* sequence,int len)
{
  
  for ( int i = 0; i < len-(word_length-1); i += 1 )
  {
    string pentapeptide = (*sequence).substr(i,word_length);
    (*pentapeptides).insert(pentapeptide);
  }
}

void getNextIterationQueries(vector< pair<long,float> > * outputIteration, queue< pair<long,string>>*queueIteration,
                            unordered_set<long>*allQueries, unordered_map<long, pair<unordered_set<string>,
                            long> >*mapSequencePeptides)
{
  vector<long> newQueries;

  pair<unordered_set<string>,long> queryPair = (*mapSequencePeptides)[-1]; 

  for(pair<long,float> p : (*outputIteration))
  {
    long id = p.first;
    float score = p.second;
    if( score < ts) 
    {
      pair<unordered_set<string>,long> p = (*mapSequencePeptides)[id];
      double lenRatio = (float) p.second / (float) queryPair.second;
      if( lenRatio > (1-tl) && lenRatio < (1+tl) )
      {
        bool isOK = true;
        for(long nId: newQueries)
        {
          if( getScore(&p.first,&(*mapSequencePeptides)[nId].first) >= ts) isOK = false;
        }
        if(!isOK) continue;
        for(long a: (*allQueries))
        {
          if( getScore(&p.first,&(*mapSequencePeptides)[a].first) >= ts) isOK = false;
        }

        if( isOK) newQueries.push_back(id);
      }
    }
  }
  
  for(long id:newQueries)
  {
    (*queueIteration).push(make_pair(id,base.baseInMemory[id].getSequence()));
  }
}

void ssearch(string * sequence, vector<long>* newIDs, vector< pair<long, float> >* results);
void convertSeq(unsigned char * alphabet, int alphabetLength,vector<string> * hits,
                                          vector< vector<unsigned char> >* seqs);

  /**
 * Function finds sequences in database
 */
void findInDatabase(void) {

  int elemPosition;
  pair<string, string>element;

  int firstSize;

  unordered_set<string>inputPeptides;


  string output="";
  string outputFasta="";
  bool first = true;
  int depth = 0;
  bool isExtended = true;

  while (queuePosition.pop(elemPosition)) {
    ofstream file,fileFasta, added;
    element = queueQuery.at(elemPosition);
    string queryDescription = element.first;
    string sequence = element.second;
    //Create out file
    string outName = queryDescription.substr(0,50);
    replace(outName.begin(),outName.end(),' ','_');
    replace(outName.begin(),outName.end(),'/','_');
    string name = outDir + outName + "_internal.txt"; //output file name
    string nameFasta = outDir + outName + "_internal_fasta.txt";
    string nameAdded = outDir + outName + "_added.txt";
    file.open(name.c_str());
    fileFasta.open(nameFasta.c_str());
    added.open(nameAdded.c_str());
    output+="Query: \n";
    output+=">" + queryDescription+"\n";output+=sequence+"\n\n";


    int len = sequence.length();
    for(int i = 0;i<len;i++)
    {
      char c = sequence.at(i);
      sequence.at(i) = toupper(c);
    }

    unordered_set<long> outIDs;

    queue< pair<long,string> > nextIterationQueries;
    unordered_map < long, pair<unordered_set<string>,long> > mapSequencePeptides; //store peptides and length of every processed sequence
    unordered_set<string> inputPeptides;

    vector< pair<long, float> > outputContainer;
    depth = 0;
    getListOfPentapeptides(&inputPeptides,&sequence,len);
    mapSequencePeptides[-1] = make_pair(inputPeptides,len);
    nextIterationQueries.push(make_pair(-1,sequence));
      vector< pair<long,float> > outputIteration;
    while(depth <= maxDepth && isExtended) {

      isExtended = false;
      unordered_set<long> allQueries;
      while( !nextIterationQueries.empty() )
      {
        isExtended = true;
        unordered_map<long, int> potentialSequence;
        //Map in which we store sequence that we hit in database
        pair<long,string> queryPair = nextIterationQueries.front();

        allQueries.insert(queryPair.first);
        //Vector in which we store ID of potential sequence which will be used in second step
        vector <long> potentialCandidate;

        nextIterationQueries.pop();
        string sequence = queryPair.second;
        unordered_set<string> peptides = mapSequencePeptides[queryPair.first].first;

        //Find hits in database. First step
        for(string peptide : peptides)
        {
          if( peptide.find("X") != string::npos || (maskChar == "N") ? (peptide.find(maskChar) != string::npos)  : false) continue;

          if ((* pentapeptideSequenceMap ).find(peptide) != (* pentapeptideSequenceMap ).end()) {
            unordered_set<long> *ID = &(*pentapeptideSequenceMap).at(peptide);

            for (unordered_set<long>::iterator it1 = (*ID).begin(); it1 != (*ID).end(); it1++) {
              int value = potentialSequence[*it1];
              potentialSequence[*it1] = value + (*base.getIndexes())[*it1].numberOfHits(peptide);

            }
          }
        }
        int min = minHits;
        int counter = 0;
        unordered_set<long>outs;

        while(true){
          for(pair<long,int> elem:potentialSequence){
            if(elem.second<min) outs.insert(elem.first);
            else counter ++;
          }
          if(counter > 12000){
            min++;
            counter = 0;
          }else break;
        }
        for(long id:outs) {
          potentialSequence[id] = 0;
        }
        for (pair<long, int> elem : potentialSequence) {
          if (elem.second > 0) {
            potentialCandidate.push_back(elem.first);
          }
        }

        //vector<long> newIDs ;//= std::copy_if(outIDs.begin(),outIDs.end(),[](long id){outIDs.find(id) != outIDs.end();});

        //for(long id:potentialCandidate){
         // if( outIDs.find(id) != outIDs.end()) continue;
          //newIDs.push_back(id);
        //}
        cout<<potentialCandidate.size()<<endl;
        //vector< pair<long,long double> > results;
        //ssearch(&sequence, & newIDs, &outputIteration);
        for(long id:potentialCandidate)
        {
          string hitSequence = base.baseInMemory[id].getSequence();
          unordered_set<string> hitPeptides;
          int len = hitSequence.length();
          getListOfPentapeptides(&hitPeptides,&hitSequence,len);
          mapSequencePeptides[id] = make_pair(hitPeptides,len);

          float score = getScore(&peptides,&hitPeptides);

          if(score >= Score_Cutoff)
          {
            float scoreWithQuery = getScore(&inputPeptides,&hitPeptides);
            if( scoreWithQuery >= Score_Cutoff) 
            {
              outputIteration.push_back(make_pair(id,scoreWithQuery));
              outIDs.insert(id);
            }
          }
        }

      }
      sort(outputIteration.begin(),outputIteration.end(),cmpa);
      if(depth>0)
      {
        for(pair<long,float> p:outputIteration)
        {
          string description = base.baseInMemory[p.first].getFirstLine();
          added<<description + "\tDepth " +to_string(depth)+"\tScore "+to_string(p.second)+"\n";
        }
      }
      reverse(outputIteration.begin(),outputIteration.end());
      outputContainer.insert(outputContainer.end(),outputIteration.begin(),outputIteration.end());
      getNextIterationQueries(&outputIteration,&nextIterationQueries,&allQueries,&mapSequencePeptides);
      depth++;
    }
    sort(outputContainer.begin(),outputContainer.end(),cmpa);
    int n = 0;
    for (pair<long, float> out : outputContainer) {
      string description = base.baseInMemory[out.first].getFirstLine();
      string sequenceO = base.baseInMemory[out.first].getSequence();
      sequence = sequenceO;
      output+=description+"\tScore: "+to_string(out.second)+"\n";
      outputFasta+=">"+description+"\t"+"Score: "+to_string(out.second)+"\n"+sequenceO+"\n\n";
      n++;
    }
    for(pair<long,int> p : outputIteration){
      string description = base.baseInMemory[p.first].getFirstLine();
      added<<description + "\tDepth: " + to_string(p.second)+"\n";
      }
    if (n == 0) {
      output+="No hits found";
      outputFasta+="No hits found";
    }
    file<<output;
    fileFasta<<outputFasta;
    output="";
    outputFasta="";
    file.close();
    fileFasta.close();
  }
}


vector<string> split(string str) {
    string::size_type loc = str.find(";;");
    string word;
    vector<string>v;

    if (loc != string::npos) {
        word = str.substr(0, loc);
        v = split(str.substr(loc + 2));
    } else {
        word = str;
    }
    v.push_back(word);
    return v;
}

void producer(string file) {
  ifstream query(file.c_str());
  string description = "";
  string sequence = "";

  string line;
  int position = 0;
  while (getline(query, line)) {
    //read description
    if (line == "") continue;
    if (line.at(0) == '>') {
      if (sequence != "") {
        pair<string, string> a = make_pair(description, sequence);
        queueQuery.push_back(a);
        while (!queuePosition.push(position))
          ;
        position++;
        sequence = "";
      }
      description = line.substr(1);
    } else {
      sequence += line;
    }
  }

  if (sequence != "" && description != "") {
    pair<string, string> a = make_pair(description, sequence);
    queueQuery.push_back(a);
    while (!queuePosition.push(position))
      ;
  }
}

int main(int argc, char * argv[]) {

  string settingsFile = "";
  string databaseType = "";

  int option;
  /*while( (option = getopt(argc,argv,"s:n:p")) >= 0) {
    switch(option){
      case 's': settingsFile = optarg;break;
      case 'n': databaseType = optarg;break;
      case 'p': databaseType = optarg;break;
    }
  }*/
  for (int i = 1; i < argc; i++) {
    string argument = argv[i];

    if (argument == "-s") {
      if (i + 1 < argc) {
        settingsFile = argv[i + 1];
      } else {
        printHelp();
        return 0;
      }
    }else if(argument == "-n" || argument=="-p")
    {
      databaseType = argument;
    }
  }

  if (settingsFile == "" || databaseType =="") {
    printHelp();
    return 0;
  }


  string pathToDatabase;
  string pathToIndexes;
  string pathToNumberOfCounts;


  ifstream settings(settingsFile.c_str());
  getline(settings, pathToDatabase);
  getline(settings, pathToIndexes);
  getline(settings, pathToNumberOfCounts);

  settings.close();


  pathToIndexes += "indexes.txt";
  pathToNumberOfCounts += "numberOfCounts.txt";

  if(databaseType == "-p"){
    base.setBase(pathToDatabase,pathToIndexes,pathToNumberOfCounts,Type::PROTEINS);
    word_length = proteinsWordLength;
    minHits = proteinsMinHits;
    maskChar = "X";
  }else{
    base.setBase(pathToDatabase,pathToIndexes,pathToNumberOfCounts,Type::NUCLEOTIDES);
    word_length = nucleotidesWordLength;
    minHits = nucleotidesMinHits;
    maskChar = "N";
  }

  printf("Database indexing, please wait!\n");


  base.checkIfIndexesExists();
  base.checkIfNumberOfCountsExists();

  boost::thread t(readDatabase);
  t.join();

  Scorer* scorer;
  int gapOpen = 3;
  int gapExt = 1;
  char* matrix = "BLOSUM_50";
  scorerCreateMatrix(&scorer,matrix,gapOpen,gapExt);
  eValueParams = createEValueParams(base.getDatabaseSize(),scorer);

  boost::asio::io_service io_service;
  tcp::acceptor acceptor(io_service, tcp::endpoint(tcp::v4(), server_port));

  boost::array<char, 500> buffer;
  cout << "Listening on port " << server_port << endl;
  while (true) {
    //reset
    numberOfSequences = 0;
    queueQuery.clear();

    tcp::socket socket(io_service);
    acceptor.accept(socket);

    string message;

    size_t len = socket.read_some(boost::asio::buffer(buffer));
    copy(buffer.begin(), buffer.begin() + len, back_inserter(message));

    vector<string> parts = split(message);
    reverse(parts.begin(), parts.end());

    string queryFile = parts.at(0);
    Score_Cutoff = atof(parts.at(1).c_str());
    outDir = parts.at(2);
    maxDepth = atoi(parts.at(3).c_str());

    cout << "New connection" << endl;
    if (outDir.at(outDir.size() - 1) != '/') {
      outDir += "/";
    }
    mkdir(outDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

    boost::thread_group workers_threads;
    boost::thread producer_thread(producer, queryFile);
    producer_thread.join();

    for (int i = 0; i < NUM_OF_CORES; i++) {
      workers_threads.create_thread(findInDatabase);
    }
    
    workers_threads.join_all();
    if (outDir.at(outDir.size() - 1) != '/') {
      outDir += "/";
    }
    cout << "Finish" << endl;
    boost::asio::write(socket, boost::asio::buffer("Finished"));
  }

}

void convertSeq(unsigned char * alphabet, int alphabetLength,vector<string> * hits, vector< vector<unsigned char> >* seqs){ 
  unsigned char letterIdx[128];
  for(int i =0;i<alphabetLength;i++){
    if (alphabet[i] == '*'){
      for( int j = 0; j < 128; j++)
        letterIdx[j] = i;
      break;
    }
  }

  for(int i = 0; i < alphabetLength; i++)
    letterIdx[alphabet[i]] = i;

  for(string seq : *hits){
    seqs->push_back(vector<unsigned char>());
    for(char& c: seq)
      seqs->back().push_back(letterIdx[c]);
  }
}

void ssearch(string * sequence, vector<long>* newIDs, vector< pair<long, float> >* results){
  int gapOpen = 3;
  int gapExt = 1;
  int modeCode = SWIMD_MODE_SW;

  ScoreMatrix scoreMatrix = ScoreMatrix::getBlosum50();
  unsigned char* alphabet = scoreMatrix.getAlphabet();
  int alphabetLength = scoreMatrix.getAlphabetLength();


  vector< vector<unsigned char> >* querySequences = new vector< vector<unsigned char> >();
  vector<string> queryS;
  queryS.push_back(*sequence);
  convertSeq(alphabet, alphabetLength, &queryS, querySequences);

  unsigned char* query = (*querySequences)[0].data();
  int queryLength = (*querySequences)[0].size();

  vector< vector<unsigned char> >* dbSequences = new vector< vector<unsigned char> >();
  
  vector<string> *seqHits = new vector<string>();

  int dbLength = 0;
  for(long id: *newIDs) {
    string seq = base.baseInMemory[id].getSequence();
   (*seqHits).push_back(seq);
   dbLength++;
  }
  convertSeq(alphabet, alphabetLength, seqHits,dbSequences);

  unsigned char** db = new unsigned char*[dbLength];
  int * dbSeqLengths = new int[dbLength];
  int dbNumResidues = 0;

  for(int i = 0; i < dbLength; i++) {
    db[i] = (*dbSequences)[i].data();
    dbSeqLengths[i] = (*dbSequences)[i].size();
    dbNumResidues += dbSeqLengths[i];
  }

  int* scores = new int[dbLength];

  swimdSearchDatabase(query, queryLength, db, dbLength, dbSeqLengths,
                      gapOpen, gapExt, scoreMatrix.getMatrix(), alphabetLength,
                      scores, modeCode, SWIMD_OVERFLOW_BUCKETS);


  int i =0;
  for(long id: *newIDs){
    float eValue = calculateEValue(scores[i],queryLength,dbSeqLengths[i],eValueParams);
    long double l = calculateEValue(scores[i],queryLength,dbSeqLengths[i],eValueParams);

    cout << scores[i]<<"\t"<<l<<endl;
    if( abs(eValue) < MAX_EVALUE){
      results->push_back(make_pair(id,eValue));
    }
    i++;
  }
}
