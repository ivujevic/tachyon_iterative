/* 
 * File:   DatabaseElement.cpp
 * Author: vujevic
 * 
 */

#include "DatabaseElement.h"

void DatabaseElement::setFirstLine(string line) {
    firstLine = line;
}

void DatabaseElement::setSequence(string sq) {
    sequence = sq;
}

void DatabaseElement::setID(long ID) {
    this->ID = ID;
}

string DatabaseElement::getFirstLine() {
    return firstLine;
}

string DatabaseElement::getSequence() {
    return sequence;
}

long DatabaseElement::getID() {
    return ID;
}
