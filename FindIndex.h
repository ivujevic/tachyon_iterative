/* 
 * File:   FindIndex.h
 * Author: vujevic
 *
 * Created on April 25, 2014, 11:42 PM
 */

#include <algorithm>
#include <vector>
#include<boost/unordered_map.hpp>

#include "SeqIndex.h"
#include "Seg.h"

using namespace std;
using namespace boost;

#ifndef FINDINDEX_H
#define	FINDINDEX_H

class FindIndex {
public:

     unordered_map<string,long> frequencyOfPentapeptide;
     void setType(Type type);
    
    /**
     * Method which crawls sequence. For every sequence method find 5 pentapeptides which represents
     * that sequence.
     */
    SeqIndex find(string sequence); 
private:
  Type type;
  int wordLength;
  int listSize;
};

#endif	/* FINDINDEX_H */
