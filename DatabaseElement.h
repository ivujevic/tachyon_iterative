/* 
 * File:   DatabaseElement.h
 * Author: vujevic
 *
 * Created on April 25, 2014, 11:43 PM
 */
#include<iostream>
#include<string>

using namespace std;

#ifndef DATABASEELEMENT_H
#define	DATABASEELEMENT_H

/**
 * Class which represents one element from database in fasta format.
 */
class DatabaseElement {
public:
    
    /**
     * Getters and setters for private members.
     */
    
    
    void setFirstLine(string line);
    void setSequence(string sq);
    void setID(long ID);
    string getFirstLine();
    string getSequence();
    long getID();
    
private:
    /**
     * First line in fasta format.
     */
    string firstLine;
    
    /**
     * Sequence
     */
    string sequence;
    
    /**
     * ID for every element in database. 
     */
    long ID;
    
    
};

#endif	/* DATABASEELEMENT_H */