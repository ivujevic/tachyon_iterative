
#ifndef EVALUE_H
#define	EVALUE_H

#include "Scorer.h"
typedef struct EValueParams EValueParams;

EValueParams* createEValueParams(long long databaseLen, Scorer* scorer);
long double calculateEValue(int score, int queryLen, int targetLen, EValueParams* params);
#endif	/* EVALUE_H */

